function Show-Menu {
  param ([string]$Title = 'My Menu')
  Clear-Host
  Write-Host "================ $Title ================"
  Write-Host "1: Indiquer le nom d'un groupe et récupérer un rapport csv contenant les membres du groupe avec leur mail, nom et prénom, "
  Write-Host "2: Lister tous les groupes contenant un mot-clef spécifique"
  Write-Host "3: Lister les comptes utilisateurs contenant un mot-clef spécifique"
  Write-Host "4: Lister les ordinateurs contenant un mot-clef spécifique"
  Write-Host "5: Lister les groupes AD vides"
  Write-Host "6: Lister les OU qui ne sont pas protégées de la suppression accidentelle"

  Write-Host "Q: Appuyer sur Q pour quitter."
}

#Vérifie si les modules Active Directory sont importés
if (-not (Import-Module activedirectory)) {Import-Module activedirectory}

# Permet de récupérer le chemin où se situe le script, pour que le rapport aille dans le chemin d'où il s'éxécute, même s'il est déplacé
$Path = (Get-Location).ProviderPath 

Do
  {
    Show-Menu –Title 'Outils Active Directory'
    $selection = Read-Host "Merci de faire votre choix"
    switch ($selection)
        {
    #Récupère le groupe AD et redemande un nom correct s'il n'existe pas
    '1' {
                Do {
                $SamAccountName = Read-Host -Prompt "Quel est le nom du groupe dont il faut récupérer le nom et le mail des membres s'il vous plaît ?"

                if ($SamAccountName -eq "") {
                Clear-Host
                Write-Host -Object "`nLe groupe '$SamAccountName' n'existe pas, veuillez indiquer un nom de groupe valide`n" -BackgroundColor Red
                continue
                }
                elseif ($(Get-ADGroup -LDAPFilter "(sAMAccountName=$SamAccountName)").SamAccountName -eq $SamAccountName) {
                $RapportGroupe = Get-ADGroup -LDAPFilter "(sAMAccountName=$SamAccountName)"

                break
                                            }
                else {
                Clear-Host
                Write-Host -Object "`nLe groupe '$SamAccountName' n'existe pas, veuillez indiquer un nom de groupe valide`n" -BackgroundColor Red
                $ToutEstOK = $false
                     }
                    }
                while ($SamAccountName -eq "" -or $ToutEstOK -eq $false)

    Get-ADGroupMember $SamAccountName | Get-ADUser -Properties Mail | Select-Object SAMAccountName, Name, givenName, Surname, Mail | Export-Csv $Path\"$SamAccountName".csv -delimiter '|' -Encoding UTF8 -NoTypeInformation
    Clear-Host
    Read-Host -Prompt “Merci ! Votre rapport se trouve dans $Path\$Groupe.csv. Appuyez sur Entrée pour quitter”
    
        }
    
    #Liste tous les groupes contenant un mot-clef spécifique
    '2' {
                $SamAccountName = Read-Host -Prompt "Quel est le mot clef que vous souhaitez indiquer ?"
                $Resultats = Get-ADGroup -Filter "SamAccountName -like '*$SamAccountName*'" -Properties SamAccountName | Select-Object SamAccountName | Sort-Object SamAccountName | Format-Table | Out-String
                Write-Host $Resultats
                Read-Host -Prompt “Voici la liste des groupes ayant le mot-clef $SamAccountName ! Appuyer sur entrée pour quitter."
        }

    #Liste les comptes utilisateurs contenant un mot-clef spécifique
    '3' {
      $SamAccountName = Read-Host -Prompt "Quel est le mot clef que vous souhaitez indiquer ?"
      $Resultats = Get-ADUser -Filter "SamAccountName -like '*$SamAccountName*'" -Properties SamAccountName | Select-Object SamAccountName | Sort-Object SamAccountName | Format-Table | Out-String
      Write-Host $Resultats
      Read-Host -Prompt “Voici la liste des utilisateurs ayant pour mot-clef $SamAccountName ! Appuyer sur entrée pour quitter."
        }

    #Liste les Ordinateurs contenant un mot-clef spécifique en indiquant leur nom, l'OS et leur adresse IP
    '4' {
        Clear-Host
        $Name = Read-Host -Prompt "Quel est le mot clef que vous souhaitez indiquer ?"
        $Resultats = Get-ADComputer -Filter "Name -like '*$Name*'" -Properties Name,OperatingSystem,IPv4Address | Select-Object Name,OperatingSystem,IPv4Address | Sort-Object Name | Format-Table | Out-String
        Write-Host $Resultats
        Read-Host -Prompt “Voici la liste des ordinateurs ayant le mot-clef $Name ! Appuyer sur entrée pour quitter."
        }


    #Liste les groupes AD vides
    '5' {
        Get-ADGroup -Filter * -Properties Members | Where {-not $_.members} | Select Name | Sort Name
            }

    #Liste les OU qui ne sont pas protégées de la suppression accidentelle
     '6' {
     Get-ADOrganizationalUnit -Filter * -Properties * | Where-Object { $_.ProtectedFromAccidentalDeletion -eq $false } | Sort-Object CanonicalName |Format-Table CanonicalName, Created
         }
    }
break
}
until ($selection -eq 'q')
