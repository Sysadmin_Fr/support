﻿Begin {
Clear-Host
#Vérifie si les modules Active Directory sont importés
if (-not (Import-Module activedirectory)) {Import-Module activedirectory}
     
# Permet de récupérer le chemin où se situe le script. Le rapport ira donc dans le chemin d'où il s'éxécute, même s'il est déplacé
$Path = (Get-Location).ProviderPath 
     
#Récupère le groupe AD et redemande un nom correct s'il n'existe pas
    do {
        $SamAccountName = Read-Host -Prompt "Quel est le nom du groupe dont il faut récupérer le nom et le mail des membres s'il vous plaît ?"
     
        if ($SamAccountName -eq "") {
            Clear-Host
            Write-Host -Object "`nLe groupe '$SamAccountName' n'existe pas, veuillez indiquer un nom de groupe valide`n" -BackgroundColor Red
            continue
        }
        elseif ($(Get-ADGroup -LDAPFilter "(sAMAccountName=$SamAccountName)").SamAccountName -eq $SamAccountName) {
            $RapportGroupe = Get-ADGroup -LDAPFilter "(sAMAccountName=$SamAccountName)"
        break
        }
        
        else {
            Clear-Host
            Write-Host -Object "`nLe groupe '$SamAccountName' n'existe pas, veuillez indiquer un nom de groupe valide`n" -BackgroundColor Red
            $ToutEstOK = $false
        }
    }
    while ($SamAccountName -eq "" -or $ToutEstOK -eq $false)
     
    Get-ADGroupMember $SamAccountName | Get-ADUser -Properties Mail | Select-Object Name, Mail | Export-Csv $Path\"$SamAccountName".csv -delimiter '|' -Encoding UTF8 -NoTypeInformation
    Clear-Host
    Read-Host -Prompt “Merci ! Votre rapport se trouve dans $Path\$Groupe.csv. Appuyez sur Entrée pour quitter” 
    }